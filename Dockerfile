FROM node:14-alpine AS builder
RUN mkdir /home/node/app
WORKDIR /home/node/app

COPY . .
RUN npm ci && npm run build

FROM registry.dev.layanan.go.id/puskomin-sipks/puppeteer-base-image:latest

RUN mkdir /home/node/app
WORKDIR /home/node/app
COPY package*.json ./

COPY --from=builder ./home/node/app/dist ./dist
COPY --from=builder ./home/node/app/pdf-templates ./pdf-templates

RUN npm ci --production

EXPOSE 3000
CMD [ "node", "dist/index.js" ]
