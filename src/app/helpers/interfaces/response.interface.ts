export interface MessageInterface {
  isSuccess: boolean;
  responseCode: number;
  responseMessage: string;
  message: string;
}

export interface ErrorMessage extends MessageInterface {
  errors: Record<string, unknown>[] | string;
}

export interface SuccessMessage extends MessageInterface {
  data: Record<string, unknown>[] | Record<string, unknown>;
}

export interface SuccessAMessage extends SuccessMessage {
  dataCount: number;
  totalData: number;
  page: number;
  totalPage: number;
}

export interface withSummaryResponse extends SuccessAMessage {
  dataSummary: Record<string, unknown>;
}

export interface JwtObject {
  access_token: string;
  expired_in: string;
}
