export interface PaginationRequest {
  take?: number;
  skip?: number;
  page?: number;
}

export interface PaginationInputRequest {
  page?: string;
  limit?: string;
}

export type User = {
  id: number;
  nip: string;
  email: string;
  password_digest: string;
  role: Role;
  institution_id: number | null;
  group_access_id: number | null;
  access_level: AccessLevel;
  created_at: Date;
  updated_at: Date;
};

export type GroupAccess = {
  id: number;
  name: string;
  description: string;
  allowed_action: Record<string, unknown>;
  created_at: Date;
  updated_at: Date;
};

export type Institution = {
  id: number;
  name: string;
  street: string;
  village: string;
  district: string;
  city: string;
  province: string;
  country: string;
  postal_code: string;
  phone_number: string;
  administration_level: UserAdministrationLevel;
  created_at: Date;
  updated_at: Date;
};

export interface LogedOnUser extends User {
  group_access: GroupAccess | null;
  institution: Institution | null;
}

export interface Regional {
  id: number;
  name: string;
  parent_id: number | null;
  code: string;
  parent_code: string;
  regional_level: string;
}

export interface RegionalRequesterData {
  provinceId?: number;
  cityId?: number;
  districtId?: number;
  villageId?: number;
}

export interface PeopleRequesterData {
  ownerId?: number;
  editorId?: number;
}

export interface UserAuthorContrib {
  id: number;
  profile: {
    fullname: string;
  };
}

export enum UserAdministrationLevel {
  PUSAT = 'PUSAT',
  PROVINSI = 'PROVINSI',
  KABUPATEN_KOTA = 'KABUPATEN_KOTA',
  KECAMATAN = 'KECAMATAN',
  KELURAHAN = 'KELURAHAN',
}

export enum Role {
  SUPER = 'SUPER',
  ADMIN = 'ADMIN',
}

export enum AccessLevel {
  ALL = 'ALL',
  SIPKS = 'SIPKS',
  PUSKOMIN = 'PUSKOMIN',
}
