export interface ConflictDataQuery {
  province?: string;
  city?: string;
  periodStart?: string;
  periodEnd?: string;
}

export interface ConflictDashboardDataQuery {
  filterByConflictDate?: string;
  filterByConflictStatus?: string;
  filterByConflictCause?: number;
}
