export interface RanpeDataQuery {
  province?: string,
  city?: string,
  admLevel?: string,
  budgetYear?: string
}