export interface FileUpload {
  id: number;
  originalname: string;
  mimetype: string;
  parent_field: string;
  parent_module: string;
  parent_id: number;
  size: number;
  created_at: Date;
  updated_at: Date;
}

export interface MultipleFileClaimRequest {
  ids: number[];
  module_id: number;
  module_name: string;
  module_field: string;
}
