export interface PdfSettings {
  templateUrl: string
  cssUrl?: string
}