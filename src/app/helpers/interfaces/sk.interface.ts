export interface SuratKeputusanList {
  id: number
  name: string
  code: string
  parent_id?: number
  parent_code?: string
  regional_level: string
  surat_keputusan: SuratKeputusan
}

export interface SuratKeputusan {
  id: number
  sk_number: string
  sk_expire_date: Date
  number_of_members: number
}