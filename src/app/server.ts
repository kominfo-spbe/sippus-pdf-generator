/* eslint-disable no-console */
import http from 'http';
import app from './config/app.config';

export default class ApplicationServer {
  private server: http.Server;
  private port: string;

  constructor() {
    this.server = new http.Server(app);
    this.port = process.env.PORT as string;
  }

  startServer(): void {
    this.server.listen(this.port);
    this.server.on('error', (err) => {
      console.log(err);
    });
    this.server.on('listening', () => {
      console.log(`Your Server Running on http://127.0.0.1:${this.port}`);
    });
  }
}
