import { NextFunction, Request, Response } from 'express';
import pdfGeneratorServices from '../services/pdf-generator.services';
import responseServices from '../services/response.services';

class GeneralCrudV1Controller {
  async download(req: Request, res: Response, next: NextFunction) {
    try {
      const generatedPdf = await pdfGeneratorServices.generate(req.pdfData, req.pdfSettings, req.pdfPageSetting)
      const fileName = req.fileName
      res.setHeader(
        'Content-Type',
        'application/pdf',
      );
      res.status(200)
      res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
      res.send(generatedPdf)
    } catch (error) {
      next(error)      
    }
  }
}

export default new GeneralCrudV1Controller();
