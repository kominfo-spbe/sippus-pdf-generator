/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { ErrorRequestHandler, NextFunction, Request, Response } from 'express';
import { ExpressJoiError } from 'express-joi-validation';
import responseServices from '../services/response.services';
import {
  AuthenticationError,
  AuthorizationError,
  ForbiddenError,
  NotExistDataError,
  UnavailableServiceError,
} from '../helpers/custom-error.class';

export const CustomJoiErrorHandler = (
  err: any,
  _req: Request,
  res: Response,
  next: NextFunction,
): void => {
  // ContainerTypes is an enum exported by this module. It contains strings
  // such as "body", "headers", "query"...
  if (err && err.type) {
    const e: ExpressJoiError = err;
    // e.g "you submitted a bad query paramater"
    switch (true) {
      case e.type == 'params' || e.type == 'query' || e.type == 'headers':
        responseServices.badRequestError(
          res,
          `Request ${e.type.toUpperCase()} Validation Error`,
          e.error?.details,
        );
        break;
      case e.type == 'body' || e.type == 'fields':
        responseServices.unprocesEntityError(
          res,
          `Request ${e.type.toUpperCase()} Validation Error`,
          e.error?.details,
        );
        break;
      default:
        responseServices.badRequestError(res, 'JSON Parse Error', e.type);
        break;
    }
  } else {
    next(err);
  }
};

// export const CustomPrismaErrorHandler = (
//   err: any,
//   _req: Request,
//   res: Response,
//   next: NextFunction,
// ): void => {
//   switch (true) {
//     case err instanceof Prisma.PrismaClientKnownRequestError:
//       responseServices.unprocesEntityError(
//         res,
//         'Validation Error',
//         err.message,
//       );
//       break;
//     case err instanceof Prisma.PrismaClientValidationError:
//       responseServices.unprocesEntityError(
//         res,
//         'Validation Error',
//         err.message,
//       );
//       break;
//     case err instanceof NotExistDataError:
//       responseServices.unprocesEntityError(
//         res,
//         'Validation Error',
//         err.message,
//       );
//       break;

//     default:
//       next(err);
//       break;
//   }
// };

export const ServiceUnavailableError = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  if (err instanceof UnavailableServiceError) {
    responseServices.unprocesEntityError(res, err.name, err.message);
  } else {
    next(err);
  }
};

export const CustomAuthError = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  switch (true) {
    case err instanceof AuthenticationError:
      responseServices.unprocesEntityError(res, err.name, err.message);
      break;
    case err instanceof AuthorizationError:
      responseServices.unauthorizedAccess(res, err.message);
      break;
    case err instanceof ForbiddenError:
      responseServices.forbiddenAccess(res, err.message);
      break;
    default:
      next(err);
      break;
  }
};

export const CustomErrorHandler: ErrorRequestHandler = (
  err,
  _req,
  res,
  _next,
) => {
  console.log(err);
  responseServices.internalServerError(res);
};
