import { NextFunction, Request, Response } from 'express';
import { AuthorizationError } from '../helpers/custom-error.class';
import externalServices from '../services/external.services';

class AuthenticationMiddleware {
  async authenticatingUser(req: Request, res: Response, next: NextFunction) {
    try {
      const authHeader = req.headers.authorization;
      if (!authHeader) {
        throw new Error();
      }
      const logedOnUser = await externalServices.authenticateUser(authHeader);
      req.authHeader = authHeader;
      req.logedOnUser = logedOnUser.data.data;
      next();
    } catch (error) {
      const newError = new AuthorizationError('Unauthorized Access');
      next(newError);
    }
  }
}

export default new AuthenticationMiddleware();
