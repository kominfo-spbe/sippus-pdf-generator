import { NextFunction, Request, Response } from 'express';
import {
  AuthorizationError,
  ForbiddenError,
} from '../helpers/custom-error.class';
import {
  Role,
  UserAdministrationLevel,
} from '../helpers/interfaces/general.interface';

class ApplicationAuthorizationMiddleware {
  authorizeRole(roles: string[]) {
    return [
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          if (!roles.includes(req.logedOnUser.role)) {
            throw new ForbiddenError('Akses Ditolak');
          }
          next();
        } catch (error) {
          next(error);
        }
      },
    ];
  }

  authorizeAccessLevel(access_level: string[]) {
    return [
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          if (!access_level.includes(req.logedOnUser.access_level)) {
            throw new ForbiddenError('Akses Ditolak');
          }
          next();
        } catch (error) {
          next(error);
        }
      },
    ];
  }

  authorizedAdmLevel(admLevel: UserAdministrationLevel[]) {
    return [
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          if (req.logedOnUser.role == 'ADMIN') {
            if (
              !req.logedOnUser.institution ||
              !admLevel.includes(
                req.logedOnUser.institution.administration_level,
              )
            ) {
              throw new ForbiddenError('Akses Ditolak');
            }
          }
          next();
        } catch (error) {
          next(error);
        }
      },
    ];
  }

  authorizeGroupAccess(module: string, action?: string) {
    return [
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          if (req.logedOnUser.role != Role.SUPER) {
            const groupAccess = req.logedOnUser.group_access;
            const allowedAccess = groupAccess?.allowed_action as Record<string, string>;
            if (
              !groupAccess ||
              !allowedAccess[module] ||
              !allowedAccess[module][action ? action : req.params.sk_type]
            ) {
              throw new ForbiddenError('Akses Ditolak');
            }
          }
          next();
        } catch (error) {
          next(error);
        }
      },
    ];
  }
}

export default new ApplicationAuthorizationMiddleware();
