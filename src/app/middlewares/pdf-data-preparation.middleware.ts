import { NextFunction, Request, Response } from 'express';
import fs from 'fs';
import path from 'path';
import moment from 'moment';
import externalServices from '../services/external.services';

class PdfDataPreparationMiddleware {
  skPdfSetup(req: Request, res: Response, next: NextFunction) {
    const now = new Date();
    const logoPath = path.resolve(
      './',
      './pdf-templates/assets/logo_large.png',
    );
    const templatePath = path.resolve(
      './',
      './pdf-templates/surat-keputusans/template.html',
    );
    const skTypeFormating = (skType: string) => {
      if (skType == 'timwasda') {
        return 'REKAPITULASI SURAT KEPUTUSAN PEMBENTUKAN <br />TIM KEWASPADAAN DINI PEMERINTAH DAERAH';
      }
      if (skType == 'fkdm') {
        return 'REKAPITULASI SURAT KEPUTUSAN PEMBENTUKAN <br />FORUM KEWASPADAAN DINI MASYARAKAT';
      }
      if (skType == 'rad') {
        return `REKAPITULASI RENCANA AKSI DAERAH <br />TAHUN ${now.getFullYear()}`;
      }
      if (skType == 'timdu') {
        return 'REKAPITULASI SURAT KEPUTUSAN PEMBENTUKAN <br />TIM TERPADU PENANGANAN KONFLIK SOSIAL';
      }
      return skType.toUpperCase();
    };
    const skTitle = skTypeFormating(req.params.skType);
    const logo = fs.readFileSync(logoPath);
    const logoBase64 = Buffer.from(logo).toString('base64');
    req.pdfData = {
      title: skTitle,
      assets: {
        logo: `data:image/png;base64, ${logoBase64}`,
      },
      data: req.resultsQuery,
    };
    req.pdfSettings = {
      templateUrl: templatePath,
    };
    req.pdfPageSetting = {
      format: 'a4',
      displayHeaderFooter: false,
      margin: {
        bottom: '80px',
        left: '80px',
        right: '80px',
        top: '80px',
      },
    };
    req.fileName = `rekapitulasi_surat_keputusan_${now
      .getTime()
      .toString()}.pdf`;
    // console.log("data", req.pdfData)
    // console.log("setting", req.pdfSettings)
    // console.log("page settings", req.pdfPageSetting)
    next();
  }

  lapsitdaPdfSetup(req: Request, res: Response, next: NextFunction) {
    const momentDate = moment(req.body.report_date).locale('id');
    const logoPath = path.resolve(
      './',
      './pdf-templates/assets/logo_large.png',
    );
    const templatePath = path.resolve(
      './',
      './pdf-templates/lapsitda/template.html',
    );
    const logo = fs.readFileSync(logoPath);
    const logoBase64 = Buffer.from(logo).toString('base64');
    req.pdfData = {
      assets: {
        logo: `data:image/png;base64, ${logoBase64}`,
      },
      data: {
        dasarHukum: req.lapsitdaDasarHukum,
        lapsitdaReport: req.resultsQuery,
        lapsitdaDate: {
          reportDate: momentDate.format('DD'),
          reportMonth: monthToRoman(momentDate.format('MM')),
          reportYear: momentDate.format('YYYY'),
          fullDate: momentDate.format('DD MMMM YYYY'),
        },
        generatedBy: {
          admLvl: req.logedOnUser.institution?.administration_level,
          province: req.logedOnUser.institution?.province,
          city: req.logedOnUser.institution?.city,
        },
      },
    };
    req.pdfSettings = {
      templateUrl: templatePath,
    };
    req.pdfPageSetting = {
      format: 'a4',
      margin: {
        bottom: '80px',
        left: '80px',
        right: '80px',
        top: '80px',
      },
    };
    req.fileName = `lapsitda-${req.body.report_date}.pdf`;
    console.dir(req.pdfData, { depth: null });
    next();
  }

  lapsitdaExclusivePdfSetup(req: Request, res: Response, next: NextFunction) {
    const logoPath = path.resolve(
      './',
      './pdf-templates/assets/logo_large.png',
    );
    const templatePath = path.resolve(
      './',
      './pdf-templates/exclusive-report/template.html',
    );
    const logo = fs.readFileSync(logoPath);
    const logoBase64 = Buffer.from(logo).toString('base64');
    req.pdfData = {
      assets: {
        logo: `data:image/png;base64, ${logoBase64}`,
      },
      data: req.resultQuery,
    };
    req.pdfSettings = {
      templateUrl: templatePath,
    };
    req.pdfPageSetting = {
      format: 'a4',
      margin: {
        bottom: '80px',
        left: '80px',
        right: '80px',
        top: '80px',
      },
    };
    // console.dir(req.pdfData, {depth: null})
    req.fileName = `laporan-khusus.pdf`;
    next();
  }

  ranpeRecapPdfSetup(req: Request, res: Response, next: NextFunction) {
    const logoPath = path.resolve(
      './',
      './pdf-templates/assets/logo_large.png',
    );
    const templatePath = path.resolve(
      './',
      './pdf-templates/ranpe-recap/template.html',
    );
    const logo = fs.readFileSync(logoPath);
    const logoBase64 = Buffer.from(logo).toString('base64');
    req.pdfData = {
      assets: {
        logo: `data:image/png;base64, ${logoBase64}`,
      },
      title: 'REKAPITULASI PENCAPAIAN PELAKSANAAN RAN PE',
      budgetYear: req.query.filterByBudgetPlanYear,
      data: req.resultsQuery,
    };
    req.pdfSettings = {
      templateUrl: templatePath,
    };
    req.pdfPageSetting = {
      format: 'a4',
      margin: {
        bottom: '80px',
        left: '80px',
        right: '80px',
        top: '80px',
      },
    };
    // console.dir(req.pdfData, {depth: null})
    req.fileName = `rekapitulai-pencapaian-pelaksanaan-ranpe${
      req.query.filterByBudgetPlanYear
        ? '-' + req.query.filterByBudgetPlanYear
        : ''
    }.pdf`;
    next();
  }

  async ranpeDetailRecapPdfSetup(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    // const logoPath = path.resolve('./', './pdf-templates/assets/logo_large.png')
    const templatePath = path.resolve(
      './',
      './pdf-templates/ranpe-recap-detail/template.html',
    );

    // const logo = fs.readFileSync(logoPath)
    // const logoBase64 = Buffer.from(logo).toString('base64')
    const region = req.resultQuery.regional;
    req.pdfData = {
      title:
        'RENCANA AKSI DAERAH PENCEGAHAN DAN PENANGGULANGAN EKSTRIMISME <br/> BERBASIS KEKERASAN YANG MENGARAH KEPADA TERORISME',
      budgetYear: req.query.filterByBudgetPlanYear,
      data: req.resultQuery,
    };
    req.pdfSettings = {
      templateUrl: templatePath,
    };
    req.pdfPageSetting = {
      format: 'a4',
      margin: {
        bottom: '.2in',
        left: '.2in',
        right: '.2in',
        top: '.2in',
      },
      landscape: true,
    };
    console.dir(region);
    console.dir(req.pdfData, { depth: null });
    req.fileName = `detil-recap-pelaksanaan-ranpe${
      region ? '-' + (region as Record<string, unknown>).code : ''
    }${
      req.query.filterByBudgetPlanYear
        ? '-' + req.query.filterByBudgetPlanYear
        : ''
    }.pdf`;
    next();
  }
  async conflictRecapPdfSetup(req: Request, res: Response, next: NextFunction) {
    const logoPath = path.resolve(
      './',
      './pdf-templates/assets/logo_large.png',
    );
    const peristiwaPath = path.resolve(
      './',
      './pdf-templates/conflicts/peristiwa.html',
    );
    const potensiPath = path.resolve(
      './',
      './pdf-templates/conflicts/potensi.html',
    );

    const logo = fs.readFileSync(logoPath);
    const logoBase64 = Buffer.from(logo).toString('base64');
    req.pdfData = {
      assets: {
        logo: `data:image/png;base64, ${logoBase64}`,
      },
      title: `REKAPITULASI ${req.params.conflictType.toUpperCase()} KONFLIK`,
      requestDateStart: req.query.filterByStartDate
        ? new Date(req.query.filterByStartDate as string)
        : null,
      requestDateEnd: req.query.filterByEndDate
        ? new Date(req.query.filterByEndDate as string)
        : null,
      data: req.resultsQuery,
    };
    req.pdfSettings = {
      templateUrl:
        req.params.conflictType == 'potensi' ? potensiPath : peristiwaPath,
    };
    req.pdfPageSetting = {
      format: 'a4',
      margin: {
        bottom: '.2in',
        left: '.2in',
        right: '.2in',
        top: '.2in',
      },
      landscape: true,
    };
    console.dir(req.pdfData, { depth: null });
    req.fileName = `rekapitulasi-${req.params.conflictType}.pdf`;
    next();
  }
  async conflictDashboardPdfSetup(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const logoPath = path.resolve(
      './',
      './pdf-templates/assets/logo_large.png',
    );
    const templatePath = path.resolve(
      './',
      './pdf-templates/conflicts/conflicts.html',
    );

    const logo = fs.readFileSync(logoPath);
    const logoBase64 = Buffer.from(logo).toString('base64');
    req.pdfData = {
      assets: {
        logo: `data:image/png;base64, ${logoBase64}`,
      },
      conflictType: req.params.conflictType.toUpperCase(),
      conflictYear: req.query.filterByConflictDate,
      data: req.resultsQuery,
    };
    req.pdfSettings = {
      templateUrl: templatePath,
    };
    req.pdfPageSetting = {
      format: 'a4',
      margin: {
        bottom: '80px',
        left: '80px',
        right: '80px',
        top: '80px',
      },
    };
    console.dir(req.pdfData, { depth: null });
    req.fileName = `rekapitulasi-${req.params.conflictType}-${req.query.filterByConflictDate}.pdf`;
    next();
  }
}

const monthToRoman = (num: string) => {
  let numberFromString = +num;
  var lookup = {
      M: 1000,
      CM: 900,
      D: 500,
      CD: 400,
      C: 100,
      XC: 90,
      L: 50,
      XL: 40,
      X: 10,
      IX: 9,
      V: 5,
      IV: 4,
      I: 1,
    },
    roman = '',
    i;
  for (i in lookup) {
    while (numberFromString >= lookup[i]) {
      roman += i;
      numberFromString -= lookup[i];
    }
  }
  return roman;
};

export default new PdfDataPreparationMiddleware();
