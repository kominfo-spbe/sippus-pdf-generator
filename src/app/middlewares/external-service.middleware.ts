import { AxiosResponse } from 'axios';
import { NextFunction, Request, Response } from 'express';
import { months } from 'moment';
import { NotExistDataError } from '../helpers/custom-error.class';
import {
  ConflictDashboardDataQuery,
  ConflictDataQuery,
} from '../helpers/interfaces/conflict.interface';
import { RanpeDataQuery } from '../helpers/interfaces/ranpe.interface';
import externalServices from '../services/external.services';

class ExternalServiceMiddleware {
  async skSetup(req: Request, res: Response, next: NextFunction) {
    try {
      const skType = req.params.skType;
      const skList = await externalServices.getSkList(req.authHeader, skType);
      req.resultsQuery = skList.data.data;
      next();
    } catch (error) {
      next(error);
    }
  }

  async lapsitdaSetup(req: Request, res: Response, next: NextFunction) {
    try {
      const lapsitdaDate = req.body.report_date;
      const [lapsitdaData, lapsitdaDh] = await Promise.all([
        await externalServices.getLapsitdaList(req.authHeader, lapsitdaDate),
        await externalServices.getLapsitdaDhList(req.authHeader),
      ]);
      req.resultsQuery = lapsitdaData.data.data;
      req.lapsitdaDasarHukum = lapsitdaDh.data.data;
      // console.dir(req.resultsQuery, { depth: null });
      next();
    } catch (error) {
      next(error);
    }
  }

  async lapsitdaExclusive(req: Request, res: Response, next: NextFunction) {
    try {
      const exclusiveId = +req.params.id;
      const exclusiveReport = await externalServices.getLapsitdaExclusive(
        req.authHeader,
        exclusiveId,
      );
      req.resultQuery = exclusiveReport.data.data;
      // console.dir(req.resultQuery, {depth: null})
      next();
    } catch (error) {
      next(error);
    }
  }

  async ranpeRecap(req: Request, res: Response, next: NextFunction) {
    try {
      const ranpeRecap = await externalServices.getRanpeRecap(
        req.authHeader,
        req.query.filterByBudgetPlanYear as string,
      );
      req.resultsQuery = ranpeRecap.data.data;
      console.dir(req.resultsQuery, { depth: null });
      next();
    } catch (error) {
      next(error);
    }
  }

  async ranpeRecapDetail(req: Request, res: Response, next: NextFunction) {
    try {
      const recapQuery: RanpeDataQuery = {
        admLevel: req.query.filterByAdministrationLevel as string,
        budgetYear: req.query.filterByBudgetPlanYear as string,
        city: req.query.filterByCity as string,
        province: req.query.filterByProvince as string,
      };
      let userRegional: string | undefined = undefined;
      switch (req.logedOnUser.institution?.administration_level) {
        case 'PROVINSI':
          userRegional = (recapQuery.city ||
            req.logedOnUser.institution.province) as string;
          break;
        case 'KABUPATEN_KOTA':
          userRegional = (recapQuery.city ||
            req.logedOnUser.institution.city) as string;
          break;
        default:
          userRegional = recapQuery.city || recapQuery.province;
          break;
      }
      const [
        regional,
        ranpeRecapDetail,
        planningMonitoring,
        activityMonitoring,
      ] = await Promise.all([
        await externalServices.getRegion(userRegional),
        await externalServices.getRanpeDetailRecap(req.authHeader, recapQuery),
        await externalServices.getRanpeAssesstDetailRecap(
          req.authHeader,
          'pemantauan-perencanaan',
          recapQuery,
        ),
        await externalServices.getRanpeAssesstDetailRecap(
          req.authHeader,
          'pemantauan-pelaksanaan',
          recapQuery,
        ),
      ]);
      console.log(
        'JUMLAH PLANNING MONITORING',
        planningMonitoring.data.data.length,
      );
      console.log(
        'JUMLAH ACTIVITY MONITORING',
        activityMonitoring.data.data.length,
      );
      req.resultQuery = {
        ranpeRecapDetail: ranpeRecapDetail.data.data,
        regional,
        planningMonitoring: planningMonitoring.data.data,
        activityMonitoring: activityMonitoring.data.data,
      };
      // console.dir(req.resultQuery)
      next();
    } catch (error) {
      next(error);
    }
  }

  async conflictRecap(req: Request, res: Response, next: NextFunction) {
    try {
      const conflictType = req.params.conflictType;
      // const dateSetup = dateExtractor(req.query.filterByConflictDate as string)
      const startDate = req.query.filterByStartDate as string;
      const endDate = req.query.filterByEndDate as string;
      console.log('THIS IS DATE SETUP', `${startDate + '-' + endDate}`);
      const dataQuery: ConflictDataQuery = {
        city: req.query.filterByCity as string,
        province: req.query.filterByProvince as string,
        periodEnd: endDate,
        periodStart: startDate,
      };
      const response = await externalServices.getConflictRecap(
        req.authHeader,
        conflictType,
        dataQuery,
      );
      req.resultsQuery = response.data.data;
      console.dir(req.resultsQuery, { depth: null });
      next();
    } catch (error) {
      next(error);
    }
  }

  async conflictDashboard(req: Request, res: Response, next: NextFunction) {
    try {
      let responseData: Record<string, unknown>[] = [];
      const conflictType = req.params.conflictType;
      const parentCode = req.query.parentCode as unknown as string;
      const dataQuery: ConflictDashboardDataQuery = {
        filterByConflictCause: req.query
          .filterByConflictCause as unknown as number,
        filterByConflictDate: req.query
          .filterByConflictDate as unknown as string,
        filterByConflictStatus: req.query
          .filterByConflictStatus as unknown as string,
      };
      if (parentCode) {
        const response =
          await externalServices.getConflictDashboardWithChildren(
            req.authHeader,
            parentCode,
            conflictType,
            dataQuery,
          );
        responseData = response.data.data;
      } else {
        const response = await externalServices.getConflictDashboard(
          req.authHeader,
          conflictType,
          dataQuery,
        );
        responseData = response.data.data;
      }
      // console.log(responseData);
      req.resultsQuery = responseData;
      next();
    } catch (error) {
      next(error);
    }
  }

  async setupLapsitdaExclusiveFiles(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    try {
      const lapsitdas = req.resultQuery.lapsitdas as Record<string, unknown>[];
      // console.log("LAPSITDAS : ", lapsitdas)
      if (lapsitdas.length > 0) {
        await Promise.all(
          (req.resultQuery.lapsitdas as Record<string, unknown>[]).map(
            async (data) => {
              const lapsitda = data.lapsitda as Record<string, unknown>;
              const imageData = lapsitda.image_attachments as number[];
              if (lapsitda && imageData && imageData.length > 0) {
                (data.lapsitda as Record<string, unknown>).image_attachments =
                  await fileProcess(imageData, req.authHeader);
              }
            },
          ),
        );
        // console.log("AFTER", lapsitdas)
      }
      // console.dir(req.resultQuery, {depth: null})
      next();
    } catch (error) {
      next(error);
    }
  }
}

const fileProcess = async (files: number[], auth: string) => {
  return await Promise.all(
    files.map(async (img) => {
      const fileRequest = await externalServices.getFile(auth, img);
      const contentType = fileRequest.headers['content-type'];
      const file = fileRequest.data;
      console.log(file);
      // console.dir(file)
      const createFile64 = Buffer.from(file).toString('base64');
      return `data:${contentType};base64, ${createFile64}`;
    }),
  );
};

const dateExtractor = (dateInput?: string) => {
  if (!dateInput) {
    return undefined;
  }
  const inputDate = new Date(dateInput);
  const month = inputDate.getMonth();
  const year = inputDate.getFullYear();
  const startDate = new Date(year, month + 1, 1).getDate();
  const endDate = new Date(year, month + 1, 0).getDate();
  return {
    outputStart: `${year}-${month + 1}-${startDate}`,
    outputEnd: `${year}-${month + 1}-${endDate}`,
  };
};

export default new ExternalServiceMiddleware();
