import { NextFunction, Request, Response } from 'express';
import generalPreparation from '../services/input-preparations/general.preparation';

class GeneralPreparationMiddleware {
  paginationInitiation(req: Request, res: Response, next: NextFunction): void {
    const dataLimit = req.query.limit as string;
    const dataPage = req.query.page as string;
    const requestPaginataion = generalPreparation.pagination(
      dataPage,
      dataLimit,
    );
    req.takeData = requestPaginataion.take;
    req.skipData = requestPaginataion.skip;
    req.pageNumber = requestPaginataion.page || 1;
    next();
  }
}

export default new GeneralPreparationMiddleware();
