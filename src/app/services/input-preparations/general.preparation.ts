import { PaginationRequest } from '../../helpers/interfaces/general.interface';

class GeneralPreparation {
  pagination(page?: string, limit?: string): PaginationRequest {
    let paginationRequest: PaginationRequest = {};
    const dataPage: number = page ? +page : 1;
    if (limit) {
      const dataLimit: number = +limit || 10;
      const dataSkip = (dataPage - 1) * dataLimit;

      paginationRequest = {
        skip: dataSkip,
        take: dataLimit,
      };
    }
    paginationRequest.page = dataPage;
    return paginationRequest;
  }
}

export default new GeneralPreparation();
