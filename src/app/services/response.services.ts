/* eslint-disable @typescript-eslint/no-explicit-any */
import { Response } from 'express';
import { StatusCodes, ReasonPhrases } from 'http-status-codes';
import {
  ErrorMessage,
  MessageInterface,
  SuccessAMessage,
  SuccessMessage,
  withSummaryResponse,
} from '../helpers/interfaces/response.interface';

class ResponseService {
  internalServerError(res: Response): void {
    const message: ErrorMessage = {
      isSuccess: false,
      responseCode: StatusCodes.INTERNAL_SERVER_ERROR,
      responseMessage: ReasonPhrases.INTERNAL_SERVER_ERROR,
      message: 'Something Wrong with Our Server',
      errors: 'call our administrator for further information',
    };
    res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(message);
  }

  unprocesEntityError(res: Response, messageInput: string, errors: any): void {
    const message: ErrorMessage = {
      isSuccess: false,
      responseCode: StatusCodes.UNPROCESSABLE_ENTITY,
      responseMessage: ReasonPhrases.UNPROCESSABLE_ENTITY,
      message: messageInput,
      errors: errors,
    };
    res.status(StatusCodes.UNPROCESSABLE_ENTITY).json(message);
  }

  badRequestError(res: Response, messageInput: string, errors: any): void {
    const message: ErrorMessage = {
      isSuccess: false,
      responseCode: StatusCodes.BAD_REQUEST,
      responseMessage: ReasonPhrases.BAD_REQUEST,
      message: messageInput,
      errors: errors,
    };
    res.status(StatusCodes.BAD_REQUEST).json(message);
  }

  dataSuccessInput(
    res: Response,
    data: Record<string, unknown>,
    messageInput?: string,
  ): void {
    const message: SuccessMessage = {
      isSuccess: true,
      responseCode: StatusCodes.OK,
      responseMessage: ReasonPhrases.OK,
      message: messageInput || 'Data Successfully Saved',
      data: data,
    };
    res.status(StatusCodes.OK).json(message);
  }

  dataSuccessArray(
    res: Response,
    data: Record<string, unknown>[],
    totalData: number,
    page: number,
    totalPage: number,
  ): void {
    const message: SuccessAMessage = {
      isSuccess: true,
      responseCode: StatusCodes.OK,
      responseMessage: ReasonPhrases.OK,
      message: data.length === 0 ? 'Data Not Found' : 'Data Found',
      page: page || 1,
      totalPage: totalPage || 0,
      totalData: totalData || 0,
      data: data,
      dataCount: data.length,
    };
    res.status(StatusCodes.OK).json(message);
  }

  summarizedDataArray(
    res: Response,
    data: Record<string, unknown>[],
    totalData: number,
    page: number,
    totalPage: number,
    summary: Record<string, unknown>,
  ): void {
    const message: withSummaryResponse = {
      isSuccess: true,
      responseCode: StatusCodes.OK,
      responseMessage: ReasonPhrases.OK,
      message: data.length === 0 ? 'Data Not Found' : 'Data Found',
      page: page || 1,
      dataCount: data.length,
      totalPage: totalPage || 0,
      totalData: totalData || 0,
      dataSummary: summary,
      data: data,
    };
    res.status(StatusCodes.OK).json(message);
  }

  dataSuccessObject(
    res: Response,
    data?: Record<string, unknown> | null | string[],
  ): void {
    const message: SuccessMessage = {
      isSuccess: true,
      responseCode: StatusCodes.OK,
      responseMessage: ReasonPhrases.OK,
      message: data ? 'Data Found' : 'Data Not Found',
      data: (data as Record<string, unknown>) || {},
    };
    res.status(StatusCodes.OK).json(message);
  }

  emptySuccessResponse(res: Response, message: string): void {
    const responseMessage: MessageInterface = {
      isSuccess: true,
      responseCode: StatusCodes.OK,
      responseMessage: ReasonPhrases.OK,
      message: message,
    };
    res.status(StatusCodes.OK).json(responseMessage);
  }

  unauthorizedAccess(res: Response, errors?: string): void {
    const message: ErrorMessage = {
      isSuccess: false,
      responseCode: StatusCodes.UNAUTHORIZED,
      responseMessage: ReasonPhrases.UNAUTHORIZED,
      message: 'Unallowed Access',
      errors: errors || 'Check your credentials',
    };
    res.status(StatusCodes.UNAUTHORIZED).json(message);
  }

  notFound(res: Response, messageIn: string, errorsIn: string): void {
    const message: ErrorMessage = {
      isSuccess: false,
      responseCode: StatusCodes.NOT_FOUND,
      responseMessage: ReasonPhrases.NOT_FOUND,
      message: messageIn,
      errors: errorsIn,
    };
    res.status(StatusCodes.NOT_FOUND).json(message);
  }

  forbiddenAccess(res: Response, messageIn: string): void {
    const message: MessageInterface = {
      isSuccess: false,
      responseCode: StatusCodes.FORBIDDEN,
      responseMessage: ReasonPhrases.FORBIDDEN,
      message: messageIn,
    };
    res.status(StatusCodes.FORBIDDEN).json(message);
  }
}

export default new ResponseService();
