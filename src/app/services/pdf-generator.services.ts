import puppeteer, { PDFOptions } from 'puppeteer';
import { PdfSettings } from '../helpers/interfaces/pdf-generator.interface';
import moment from 'moment';
import 'moment/locale/id';
import fs from 'fs';
import handlebars from 'handlebars';

class PdfGenerator {
  haeadlesChromeWs?: string;

  async generate(
    data: Record<string, unknown>,
    pdfSettings: PdfSettings,
    pdfPageSetting: PDFOptions,
  ) {
    const args = ['--no-sandbox', '--disable-setuid-sandbox'];
    handlebars.registerHelper('inc', function (value, options) {
      return parseInt(value) + 1;
    });
    handlebars.registerHelper('currencyFormat', function (value: string) {
      if (!value) return 0;
      // Helper parameters
      const dl = 2;
      const ts = ',';
      const ds = '.';

      // Parse to float
      const newValue: number = parseFloat(value);

      // The regex
      const re = '\\d(?=(\\d{3})+' + (dl > 0 ? '\\D' : '$') + ')';

      // Formats the number with the decimals
      const num = newValue.toFixed(Math.max(0, ~~dl));

      // Returns the formatted number
      return (ds ? num.replace('.', ds) : num).replace(
        new RegExp(re, 'g'),
        '$&' + ts,
      );
    });

    handlebars.registerHelper('korbanHelper', function (value) {
      if (!value) {
        return 'NIHIL';
      }
      return `${value} orang`;
    });
    handlebars.registerHelper('arrayCountHelper', function (value) {
      if (!value || value.length == 0) {
        return 0;
      }
      return value.length;
    });
    handlebars.registerHelper('toPercentage', function (value) {
      if (!parseFloat(value)) {
        return '';
      }
      return `( ${value} % )`;
    });

    handlebars.registerHelper(
      'findMonthAndReturnCurrency',
      function (value, month) {
        const dataObject = value.find((data) => {
          if (data.month == month) {
            return data;
          }
        });
        if (dataObject) {
          return dataObject.budget;
        }
        return 0;
      },
    );

    handlebars.registerHelper('isB06', function (value) {
      return value === 'B06';
    });
    handlebars.registerHelper('nihilReturn', function (value) {
      if (!value) {
        return 'Nihil';
      }
      return value;
    });
    handlebars.registerHelper(
      'lapsitdaLetterNumber',
      function (generatedBy, value) {
        if (generatedBy.admLvl == 'KABUPATEN_KOTA') {
          return `${value.reportDate}/LAPSITDA/PUSKOMIN/${generatedBy.city}/${value.reportMonth}/${value.reportYear}`;
        }
        if (generatedBy.admLvl == 'PROVINSI') {
          return `${value.reportDate}/LAPSITDA/PUSKOMIN/${generatedBy.province}/${value.reportMonth}/${value.reportYear}`;
        } else {
          return `${value.reportDate}/LAPSITDA/PUSKOMIN/${value.reportMonth}/${value.reportYear}`;
        }
      },
    );
    handlebars.registerHelper('lapsitdaHeaderCheck', function (generatedBy) {
      if (generatedBy.admLvl != 'PUSAT') {
        return true;
      }
      return false;
    });

    handlebars.registerHelper('dateFormat', function (value) {
      return moment(value, moment.ISO_8601).locale('id').format('DD MMMM YYYY');
    });

    handlebars.registerHelper('conflictStartDateFormat', function (value) {
      console.log(value);
      if (!value) return '';
      const date = moment(value, moment.ISO_8601)
        .locale('id')
        .format('DD MMMM YYYY');
      // const year = moment(value, moment.ISO_8601).locale('id').format('YYYY')
      return `${date}`;
    });
    handlebars.registerHelper('conflictEndDateFormat', function (value) {
      console.log(value);
      if (!value) return '';
      const date = moment(value, moment.ISO_8601)
        .locale('id')
        .format('DD MMMM YYYY');
      // const year = moment(value, moment.ISO_8601).locale('id').format('YYYY')
      return `HINGGA ${date}`;
    });
    const file = fs.readFileSync(pdfSettings.templateUrl, 'utf-8');
    const toBeBindTemplate = handlebars.compile(file);
    const bindedHtml = toBeBindTemplate(data);
    console.log('headless Chrome', this.haeadlesChromeWs);
    if (!this.haeadlesChromeWs) {
      const chromeBrowser = await puppeteer.launch({
        executablePath: '/usr/bin/google-chrome',
        args: args,
      });
      this.haeadlesChromeWs = chromeBrowser.wsEndpoint();
    }
    const browser = await puppeteer.connect({
      browserWSEndpoint: this.haeadlesChromeWs,
    });
    const page = await browser.newPage();
    await page.setContent(bindedHtml, { waitUntil: 'networkidle0' });
    const generatedPdf = await page.pdf(pdfPageSetting);
    return generatedPdf;
  }
}

export default new PdfGenerator();
