import axios from 'axios';
import {
  ConflictDashboardDataQuery,
  ConflictDataQuery,
} from '../helpers/interfaces/conflict.interface';
import { MultipleFileClaimRequest } from '../helpers/interfaces/file.interface';
import { Regional } from '../helpers/interfaces/general.interface';
import { RanpeDataQuery } from '../helpers/interfaces/ranpe.interface';

class ExternalService {
  async authenticateUser(authHeader: string) {
    const authUrl = `http://${process.env.USER_SERVICE}/authenticate/v1/me`;
    // const authUrl = `${process.env.USER_SERVICE}/authenticate/v1/me`;
    const response = await axios.get(authUrl, {
      headers: {
        Authorization: authHeader,
      },
    });
    return response;
  }

  async getUser(authHeader: string, accessLevel: string, id?: number | null) {
    if (!id) {
      return null;
    }
    const userUrl = `http://${process.env.USER_SERVICE}/services/v1/users/${accessLevel}/${id}`;
    const response = await axios.get(userUrl, {
      headers: {
        Authorization: authHeader,
      },
      params: {
        minQuery: true,
      },
    });
    return response;
  }

  async getSkList(authHeader: string, skType: string) {
    const listSkUrl = `http://${process.env.SK_SERVICE}/surat-keputusans/v1/${skType}`;
    const response = await axios.get(listSkUrl, {
      headers: {
        Authorization: authHeader,
      },
    });

    return response;
  }

  async getLapsitdaDhList(authHeader: string) {
    const lapsitdaDhUrl = `http://${process.env.LAPSITDA_SERVICE}/lapsitda/dasar-hukum/v1`;
    const response = await axios.get(lapsitdaDhUrl, {
      headers: {
        Authorization: authHeader,
      },
    });
    return response;
  }

  async getLapsitdaList(authHeader: string, date: string) {
    const lapsitdaReportUrl = `http://${process.env.LAPSITDA_SERVICE}/lapsitda/v1/generate-reports-service`;
    const response = await axios.post(
      lapsitdaReportUrl,
      {
        report_date: date,
      },
      {
        headers: {
          Authorization: authHeader,
        },
      },
    );
    return response;
  }

  async getLapsitdaExclusive(authHeader: string, id: number) {
    const exclusiveReportUrl = `http://${process.env.LAPSITDA_SERVICE}/lapsitda/laporan-khusus/v1/${id}`;
    const response = await axios.get(exclusiveReportUrl, {
      headers: {
        Authorization: authHeader,
      },
    });

    return response;
  }

  async getRanpeRecap(authHeader: string, budgetYear?: string) {
    const ranpeRecapUrl = `http://${process.env.RANPE_SERVICE}/ranpe/recap/v1/download`;
    const response = await axios.get(ranpeRecapUrl, {
      headers: {
        Authorization: authHeader,
      },
      params: {
        filterByBudgetPlanYear: budgetYear,
      },
    });
    return response;
  }

  async getRanpeDetailRecap(authHeader: string, dataQuery?: RanpeDataQuery) {
    const ranpeRecapDetailUrl = `http://${process.env.RANPE_SERVICE}/ranpe/recap/v1/download/details`;
    const response = await axios.get(ranpeRecapDetailUrl, {
      headers: {
        Authorization: authHeader,
      },
      params: {
        filterByProvince: dataQuery?.province,
        filterByCity: dataQuery?.city,
        filterByAdministrationLevel: dataQuery?.admLevel,
        filterByBudgetPlanYear: dataQuery?.budgetYear,
      },
    });
    return response;
  }

  async getRanpeAssesstDetailRecap(
    authHeader: string,
    assessmentType: string,
    dataQuery?: RanpeDataQuery,
  ) {
    const assessmentRecapUrl = `http://${process.env.RANPE_SERVICE}/ranpe/assessment/v1/${assessmentType}/download`;
    const response = await axios.get(assessmentRecapUrl, {
      headers: {
        Authorization: authHeader,
      },
      params: {
        filterByProvince: dataQuery?.province,
        filterByCity: dataQuery?.city,
        filterByAdministrationLevel: dataQuery?.admLevel,
        filterByBudgetPlanYear: dataQuery?.budgetYear,
      },
    });
    return response;
  }

  async getConflictRecap(
    authHeader: string,
    conflictType: string,
    dataQuery?: ConflictDataQuery,
  ) {
    const conflictUrl = `http://${process.env.CONFLICT_SERVICE}/conflicts/v1/${conflictType}/download`;
    const response = await axios.get(conflictUrl, {
      headers: {
        Authorization: authHeader,
      },
      params: {
        filterByProvince: dataQuery?.province,
        filterByCity: dataQuery?.city,
        filterByCldStart: dataQuery?.periodStart,
        filterByCldEnd: dataQuery?.periodEnd,
      },
    });
    return response;
  }

  async getConflictDashboard(
    authHeader: string,
    conflictType: string,
    dataQuery?: ConflictDashboardDataQuery,
  ) {
    const conflictUrl = `http://${process.env.DASHBOARD_CONFLICT_SERVICE}/dashboards/sipks/v1/conflicts/${conflictType}`;
    const response = await axios.get(conflictUrl, {
      headers: {
        Authorization: authHeader,
      },
      params: {
        filterByConflictDate: dataQuery?.filterByConflictDate,
        filterByConflictStatus: dataQuery?.filterByConflictStatus,
        filterByConflictCause: dataQuery?.filterByConflictCause,
      },
    });
    console.dir(response.data.data, { depth: null });
    return response;
  }
  async getConflictDashboardWithChildren(
    authHeader: string,
    parentCode: string,
    conflictType: string,
    dataQuery?: ConflictDashboardDataQuery,
  ) {
    const conflictUrl = `http://${process.env.DASHBOARD_CONFLICT_SERVICE}/dashboards/sipks/v1/conflicts/${conflictType}/regional/${parentCode}/childrens`;
    const response = await axios.get(conflictUrl, {
      headers: {
        Authorization: authHeader,
      },
      params: {
        filterByConflictDate: dataQuery?.filterByConflictDate,
        filterByConflictStatus: dataQuery?.filterByConflictStatus,
        filterByConflictCause: dataQuery?.filterByConflictCause,
      },
    });
    console.dir(response.data.data, { depth: null });
    return response;
  }

  // async getUserRegions(whereClause: string, limit?: number, skip?: number) {
  //   whereClause += `LIMIT ${limit || 'ALL'}`;
  //   const selectClause =
  //     'id, name, code, parent_id, regional_level, parent_code';
  //   if (skip) whereClause += `OFFSET ${skip}`;
  //   const data = await prisma.$queryRaw<Regional[]>`SELECT ${Prisma.raw(
  //     selectClause,
  //   )} FROM public.regions ${Prisma.raw(whereClause)}`;

  //   return data;
  // }

  async getRegion(id?: string) {
    if (id) {
      const regionUrl = `http://${process.env.REGION_SERVICE}/regionals/v1/provinsi/${id}`;
      const response = await axios.get(regionUrl);
      return response.data.data;
    }
  }

  async getFile(authHeader: string, id: number) {
    const fileUrl = `http://${process.env.UPLOADER_SERVICE}/file-uploads/v1/file/${id}`;
    const response = await axios.get(fileUrl, {
      responseType: 'arraybuffer',
      headers: {
        Authorization: authHeader,
      },
    });
    // console.log(response);
    return response;
  }

  // async claimFiles(authHeader: string, fileRequest: MultipleFileClaimRequest) {
  //   const fileClaimUrl = `http://${process.env.UPLOADER_SERVICE}/file-uploads/services/v1/multiple/claims`;
  //   const response = await axios.post(fileClaimUrl, fileRequest, {
  //     headers: {
  //       Authorization: authHeader,
  //     },
  //   });
  //   return response;
  // }
}

export default new ExternalService();
