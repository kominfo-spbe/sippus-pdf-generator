import express, { Request, Response } from 'express';
import cors from 'cors';
import morgan from 'morgan';
import {
  CustomAuthError,
  CustomErrorHandler,
  CustomJoiErrorHandler,
  ServiceUnavailableError,
} from '../middlewares/error-handler.middleware';
import responseServices from '../services/response.services';
import authenticationMiddleware from '../middlewares/authentication.middleware';
import pdfGeneratorRoute from '../routes/pdf-generator.route';

const app = express();

app.use(cors());
app.set('trust proxy', 1);
app.disable('x-powered-by');
app.use(express.json({ limit: '50mb' }));
app.use(morgan('common'));

app.get('/pdf-generators', (_req: Request, res: Response) => {
  const suratKeputusan = {
    message: 'OK',
    title: 'PDF Generators Modules Microservice API',
    version: '1.0',
    date: new Date(),
    documentations: '',
  };
  res.status(200).json(suratKeputusan);
});

app.use(
  '/pdf-generators/v1',
  authenticationMiddleware.authenticatingUser,
  pdfGeneratorRoute.v1Init(),
);

app.use('*', (_req: Request, res: Response) => {
  responseServices.notFound(res, 'Routing Error', 'No Such Endpoint');
});

app.use(CustomJoiErrorHandler);
app.use(ServiceUnavailableError);
app.use(CustomAuthError);
app.use(CustomErrorHandler);

export default app;
