import { Router } from 'express';
import generalCrudV1Controller from '../controllers/general-crud.v1.controller';
import externalServiceMiddleware from '../middlewares/external-service.middleware';
import pdfDataPreparationMiddleware from '../middlewares/pdf-data-preparation.middleware';
import externalServices from '../services/external.services';
import MainRoute from './main.route';

class PdfGeneratorRoute extends MainRoute {
  constructor() {
    super();
  }

  v1Init(): Router {
    this.router.post(
      '/lapsitda-reports',
      externalServiceMiddleware.lapsitdaSetup,
      pdfDataPreparationMiddleware.lapsitdaPdfSetup,
      generalCrudV1Controller.download,
    );
    this.router.get(
      '/surat-keputusans/:skType',
      externalServiceMiddleware.skSetup,
      pdfDataPreparationMiddleware.skPdfSetup,
      generalCrudV1Controller.download,
    );

    this.router.get(
      '/lapsitda/laporan-khusus/:id',
      externalServiceMiddleware.lapsitdaExclusive,
      externalServiceMiddleware.setupLapsitdaExclusiveFiles,
      pdfDataPreparationMiddleware.lapsitdaExclusivePdfSetup,
      generalCrudV1Controller.download,
    );

    this.router.get(
      '/ranpe/recap',
      externalServiceMiddleware.ranpeRecap,
      pdfDataPreparationMiddleware.ranpeRecapPdfSetup,
      generalCrudV1Controller.download,
    );

    this.router.get(
      '/ranpe/recap/detail',
      externalServiceMiddleware.ranpeRecapDetail,
      pdfDataPreparationMiddleware.ranpeDetailRecapPdfSetup,
      generalCrudV1Controller.download,
    );

    this.router.get(
      '/conflicts/recap/:conflictType',
      externalServiceMiddleware.conflictRecap,
      pdfDataPreparationMiddleware.conflictRecapPdfSetup,
      generalCrudV1Controller.download,
    );

    this.router.get(
      '/conflicts/dashboard/:conflictType',
      externalServiceMiddleware.conflictDashboard,
      pdfDataPreparationMiddleware.conflictDashboardPdfSetup,
      generalCrudV1Controller.download,
    );

    return this.router;
  }
}

export default new PdfGeneratorRoute();
