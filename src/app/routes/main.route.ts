import express, { Router } from 'express';
import { createValidator, ExpressJoiInstance } from 'express-joi-validation';

export default class MainRoute {
  protected validator: ExpressJoiInstance;
  protected router: Router;
  constructor() {
    this.router = express.Router();
    this.validator = createValidator({ passError: true });
  }
}
