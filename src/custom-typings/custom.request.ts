/* eslint-disable @typescript-eslint/no-namespace */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { PDFOptions } from 'puppeteer';
import {
  LogedOnUser,
  Regional,
  UserAuthorContrib,
} from '../app/helpers/interfaces/general.interface';
import { PdfSettings } from '../app/helpers/interfaces/pdf-generator.interface';

declare global {
  namespace Express {
    interface Request {
      // Auth Custom Request Interface
      logedOnUser: LogedOnUser;
      // General Custom Request Interface
      resultQuery: Record<string, unknown>;
      pdfData: Record<string, unknown>;
      pdfSettings: PdfSettings
      pdfPageSetting: PDFOptions
      resultsQuery: Record<string, unknown>[];
      fileName: string
      lapsitdaDasarHukum: Record<string, unknown>;
      lapsitdaDate: Record<string, unknown>;
      responseMessage: string;
      takeData?: number;
      skipData?: number;
      pageNumber: number | 1;
      dataTotal: number | 0;
      totalPage: number | 1;
      authHeader: string;
      // dataSummary: Record<string, unknown>;
    }
  }
}
